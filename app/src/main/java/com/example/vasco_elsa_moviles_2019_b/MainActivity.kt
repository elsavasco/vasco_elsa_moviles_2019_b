package com.example.vasco_elsa_moviles_2019_b

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import java.util.*
import kotlin.concurrent.timer

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val  bInicio=findViewById<Button>(R.id.idBotonInicio)
        val bPuntos= findViewById<Button>(R.id.idPuntos)
        val count

        val T = Timer() T.scheduleAtFixedRate object : TimerTask() {
            fun run() {
                myTextView.setText("count=$count")
                count++
            }
        }, 1000, 1000)

        bInicio.setOnClickListener({
            Toast.makeText(this, "${Random().nextInt((10 - 0)) + 1}", Toast.LENGTH_LONG).show()

        })

        bPuntos.setOnClickListener {
            if (true){
                Toast.makeText(this, "100", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this, "puntos extras", Toast.LENGTH_LONG).show()
            }


        }


    }
}
